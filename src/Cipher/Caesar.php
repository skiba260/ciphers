<?php

declare(strict_types=1);

namespace App\Cipher;

use App\CiphersContract;


class Caesar implements CiphersContract
{
    private $shiftEncodeNumber;
    private $shiftDecodeNumber;

    /**
     * Caesar constructor.
     * @param int $shiftEncodeNumber
     */
    public function __construct(int $shiftEncodeNumber = 3)
    {
        if(0 >= $shiftEncodeNumber) {
            throw new \InvalidArgumentException('The shiftEncodeNumber argument must be positive number');
        }

        $this->shiftEncodeNumber = $shiftEncodeNumber;
        $this->shiftDecodeNumber = -$shiftEncodeNumber;
    }

    public function encrypt(string $input): string
    {
        $letterToEncrypt = $this->getFirstLetter($input);
        $encryptedLetter = $this->getChangedLetter($letterToEncrypt['MATCH_RESULT'], $this->shiftEncodeNumber);

        return $this->replaceLetterInString($input, $encryptedLetter, $letterToEncrypt['LETTER_POSITION']);

    }

    public function decrypt(string $input): string
    {
        $letterToDecrypt = $this->getFirstLetter($input);
        $decryptedLetter = $this->getChangedLetter($letterToDecrypt['MATCH_RESULT'], $this->shiftDecodeNumber);

        return $this->replaceLetterInString($input, $decryptedLetter, $letterToDecrypt['LETTER_POSITION']);
    }

    private function getFirstLetter(string $input): array
    {
        if (preg_match('/[a-z]/i', $input, $result, PREG_OFFSET_CAPTURE)) {
            return [ 'MATCH_RESULT' => $result[0][0], 'LETTER_POSITION' => $result[0][1]];
        }
    }

    private function getChangedLetter(string $letter, int $shiftNumber): string
    {
        if($this->isPolishLetter($letter)) {
            throw new \InvalidArgumentException('Polish characters are not supported');
        }

        $shift   = $shiftNumber % 25;
        $ascii   = ord($letter);
        $shifted = $ascii + $shift;

        return chr($shifted);
    }

    private function isPolishLetter(string $letter) : bool
    {
        preg_match('/[żźćńółęąśŻŹĆĄŚĘŁÓŃ]*/u', $letter, $result,PREG_UNMATCHED_AS_NULL);
        return (false === empty($result[0]));
    }

    private function replaceLetterInString(string $input, string $encryptedLetter, int $position): string
    {
        $inputLetters = str_split($input);
        $inputLetters[$position] = $encryptedLetter;

        return implode($inputLetters);
    }

}