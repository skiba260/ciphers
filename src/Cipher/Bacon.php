<?php

declare(strict_types=1);

namespace App\Cipher;

use App\CiphersContract;


class Bacon implements CiphersContract
{
    private const COUNT_BACON_ENCRYPT_LENGTH = 5;
    private $dictionary = [
        'A' => 'aaaaa',
        'B' => 'aaaab',
        'C' => 'aaaba',
        'D' => 'aaabb',
        'E' => 'aabaa',
        'F' => 'aabab',
        'G' => 'aabba',
        'H' => 'aabbb',
        'I' => 'abaaa',
        'J' => 'abaab',
        'K' => 'ababa',
        'L' => 'ababb',
        'M' => 'abbaa',
        'N' => 'abbab',
        'O' => 'abbba',
        'P' => 'abbbb',
        'Q' => 'baaaa',
        'R' => 'baaab',
        'S' => 'baaba',
        'T' => 'baabb',
        'U' => 'babaa',
        'V' => 'babab',
        'W' => 'babba',
        'X' => 'babbb',
        'Y' => 'bbaaa',
        'Z' => 'bbaab'
    ];


    public function encrypt(string $input): string
    {
        $letterToEncrypt = $this->getFirstLetter($input);
        $encryptedLetter = $this->getEncryptedLetter($letterToEncrypt['MATCH_RESULT']);

        return $this->replaceLetterInString($input, $encryptedLetter, $letterToEncrypt['LETTER_POSITION']);

    }

    public function decrypt(string $input): string
    {
        $letterToDecrypt = $this->getEncryptedFirstLetter($input);
        $decryptedLetter = $this->getDecryptedLetter($letterToDecrypt);

        return $this->replaceLetterInEncodedString($input, $letterToDecrypt, $decryptedLetter);
    }

    private function getFirstLetter(string $input): array
    {
        if (preg_match('/[a-z]/i', $input, $result, PREG_OFFSET_CAPTURE)) {
            return [ 'MATCH_RESULT' => $result[0][0], 'LETTER_POSITION' => $result[0][1] ];
        }
    }

    private function getEncryptedFirstLetter(string $input): string
    {
        $firstLetter = $this->getFirstLetter($input);
        return substr($input, $firstLetter['LETTER_POSITION'],$firstLetter['LETTER_POSITION'] + self::COUNT_BACON_ENCRYPT_LENGTH);
    }

    private function getEncryptedLetter(string $letter): string
    {
        if(false === array_key_exists($letter, $this->dictionary)) {
            throw new \InvalidArgumentException('The letter does not belong to the latin alphabet');
        }

        return $this->dictionary[$letter];
    }

    private function getDecryptedLetter(string $encryptedLetter): string
    {
        if(false === in_array($encryptedLetter, $this->dictionary, true)) {
            throw new \InvalidArgumentException('The encrypted letter is not valid.');
        }

        return array_search($encryptedLetter, $this->dictionary, true);
    }

    private function replaceLetterInString(string $input, string $encryptedLetter, int $position): string
    {
        $inputLetters = str_split($input);
        $inputLetters[$position] = $encryptedLetter;

        return implode($inputLetters);
    }

    private function replaceLetterInEncodedString(string $input, string $encryptedLetter, string $decryptedLetter): string
    {
       return str_replace($encryptedLetter, $decryptedLetter, $input);
    }

}