<?php

declare(strict_types=1);

namespace App\Cipher;

use App\CiphersContract;


class AtBash implements CiphersContract
{

    private $lettersDictionaryFromBegin;
    private $lettersDictionaryFromEnd;

    public function __construct()
    {
        $this->lettersDictionaryFromBegin = range('a', 'z');
        $this->lettersDictionaryFromEnd   = range('z', 'a');
    }

    public function encrypt(string $input): string
    {
        $letterToEncrypt = $this->getFirstLetter($input);
        $encryptedLetter = $this->getEncryptedLetter($letterToEncrypt['MATCH_RESULT']);

        return $this->replaceLetterInString($input, $encryptedLetter, $letterToEncrypt['LETTER_POSITION']);

    }

    public function decrypt(string $input): string
    {
        $letterToDecrypt = $this->getFirstLetter($input);
        $decryptedLetter = $this->getDecryptedLetter($letterToDecrypt['MATCH_RESULT']);

        return $this->replaceLetterInString($input, $decryptedLetter, $letterToDecrypt['LETTER_POSITION']);
    }

    private function getFirstLetter(string $input): array
    {
        if (preg_match('/[a-z]/i', $input, $result, PREG_OFFSET_CAPTURE)) {
            return [ 'MATCH_RESULT' => $result[0][0], 'LETTER_POSITION' => $result[0][1]];
        }
    }

    private function getEncryptedLetter(string $letter): string
    {
        if(false === in_array(mb_strtolower($letter), $this->lettersDictionaryFromEnd, true)) {
            throw new \InvalidArgumentException('The letter does not belong to the latin alphabet');
        }

        return $this->lettersDictionaryFromEnd[array_search(mb_strtolower($letter), $this->lettersDictionaryFromBegin, true)];

    }

    private function getDecryptedLetter(string $encryptedLetter): string
    {
        if(false === in_array(mb_strtolower($encryptedLetter), $this->lettersDictionaryFromBegin, true)) {
            throw new \InvalidArgumentException('The encrypted letter is not valid.');
        }

        return $this->lettersDictionaryFromBegin[array_search(mb_strtolower($encryptedLetter), $this->lettersDictionaryFromEnd, true)];

    }

    private function replaceLetterInString(string $input, string $encryptedLetter, int $position): string
    {
        $inputLetters = str_split($input);
        $inputLetters[$position] = $encryptedLetter;

        return implode($inputLetters);
    }

}