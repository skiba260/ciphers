<?php

use App\Cipher\Caesar;
use App\CiphersContract;
use PHPUnit\Framework\TestCase;


class CaesarTest extends TestCase
{

    public function testImplementCiphersContract(): void
    {
        $this->assertInstanceOf(CiphersContract::class, new Caesar());
    }

    public function testEncrypt(): void
    {
        $bacon = new Caesar();
        $encrypted = $bacon->encrypt('Cezar');

        $this->assertStringStartsWith('F', $encrypted);
    }

    public function testDecrypt(): void
    {
        $bacon = new Caesar();
        $encrypted = $bacon->encrypt('Cezar');
        $decrypted = $bacon->decrypt($encrypted);
        $this->assertStringStartsWith('C', $decrypted);
    }
}