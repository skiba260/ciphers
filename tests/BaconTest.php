<?php

use App\Cipher\Bacon;
use App\CiphersContract;
use PHPUnit\Framework\TestCase;


class BaconTest extends TestCase
{

    public function testImplementCiphersContract(): void
    {
        $this->assertInstanceOf(CiphersContract::class, new Bacon());
    }

    public function testEncrypt(): void
    {
        $bacon = new Bacon();
        $encrypted = $bacon->encrypt('Cezar');

        $this->assertStringStartsWith('aaaba', $encrypted);
    }

    public function testDecrypt(): void
    {
        $bacon = new Bacon();
        $encrypted = $bacon->encrypt('Cezar');
        $decrypted = $bacon->decrypt($encrypted);
        $this->assertStringStartsWith('C', $decrypted);
    }
}