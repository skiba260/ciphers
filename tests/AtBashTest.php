<?php

use App\Cipher\AtBash;
use App\CiphersContract;
use PHPUnit\Framework\TestCase;


class AtBashTest extends TestCase
{

    public function testImplementCiphersContract(): void
    {
        $this->assertInstanceOf(CiphersContract::class, new AtBash());
    }

    public function testEncrypt(): void
    {
        $atbash = new AtBash();
        $encrypted = $atbash->encrypt('Cezar');

        $this->assertStringStartsWith('x', $encrypted);
    }

    public function testDecrypt(): void
    {
        $atbash = new AtBash();
        $encrypted = $atbash->encrypt('Cezar');
        $decrypted = $atbash->decrypt($encrypted);
        $this->assertStringStartsWith('c', $decrypted);
    }
}